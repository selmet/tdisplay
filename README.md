# tdisplay

## Description
A set of utilities for for importing datasets from external files and for displaying the contents of data frames.

## Name
Tool Box for Data Importation and Display

## Installation in RStudio

So as to install the package tdisplay directly from the remote repository gitlab, you need to ensure that you have Git software installed on your system. More details [here](https://support.posit.co/hc/en-us/articles/200532077)

Then you use install_git from the package [remotes.](https://CRAN.R-project.org/package=remotes) 

```R
install.packages("remotes")
library(remotes)
```

for the current developing version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/tdisplay')
```
or for a specific version:
```R
remotes::install_git('https://gitlab.cirad.fr/selmet/tdisplay', ref = "1.0.1")
```

## Support
please send a message to the maintainer: Samir Messad <samir.messad@cirad.fr>

## Authors
Matthieu Lesnoff <matthieu.lesnoff@cirad.fr>, Samir Messad <samir.messad@cirad.fr> and Renaud Lancelot <renaud.lancelot@cirad.fr>

## License
[GPL (>= 3)](https://www.gnu.org/licenses/gpl-3.0.html)

## Project status
This package was originally developed to make it easier to learn R software. This included importing data files, preparing them and carrying out simple statistical analyses. At the time, there were very few resources available to help you take your first steps with R software. This is no longer the case today, and there are a very large number of tutorials, guides and videos to help you through this crucial stage. This package is therefore no longer under development, but we are continuing to maintain it so that it can continue to help those who use or will use this package.
